from typing import Dict, Optional, Union

import logging
import numpy
from overrides import overrides
import torch
from torch import nn
import torch.nn.functional as F

from allennlp.common import Params
from allennlp.common.checks import check_dimensions_match, ConfigurationError
from allennlp.data import Vocabulary
from allennlp.modules import Elmo, FeedForward, Maxout, Seq2SeqEncoder, TextFieldEmbedder
from allennlp.models.model import Model
from allennlp.nn import InitializerApplicator, RegularizerApplicator
from allennlp.nn import util
from allennlp.training.metrics import CategoricalAccuracy

logger = logging.getLogger(__name__)


@Model.register("dan")
class DeepAveragingNetwork(Model):
    def __init__(self,
                 vocab: Vocabulary,
                 embedding_dropout: int,
                 text_field_embedder: TextFieldEmbedder,
                 feedforward: FeedForward,
                 logits_fn: FeedForward,
                 initializer: InitializerApplicator = InitializerApplicator()) -> None:
        super(DeepAveragingNetwork, self).__init__(vocab)

        self._text_field_embedder = text_field_embedder
        self._embedding_dropout = nn.Dropout(embedding_dropout)
        self._feedforward = feedforward
        self._logits_fn = logits_fn

        self._softmax = nn.LogSoftmax()
        self._accuracy = CategoricalAccuracy()
        self._loss = torch.nn.CrossEntropyLoss()

        initializer(self)

    @overrides
    def forward(self,  # type: ignore
                sentence: Dict[str, torch.LongTensor],
                label: torch.LongTensor = None) -> Dict[str, torch.Tensor]:
        mask = util.get_text_field_mask(sentence).float().unsqueeze(-1)
        embedded_text = self._text_field_embedder(sentence)
        dropped_embedded_text = self._embedding_dropout(embedded_text)
        averaged_embeddings = util.masked_mean(dropped_embedded_text, mask, dim=1)

        projected_embeddings = self._feedforward(averaged_embeddings)
        logits = self._logits_fn(projected_embeddings)
        probs = nn.functional.softmax(logits, dim=-1)

        output_dict = {'logits': logits, 'class_probabilities': probs}
        if label is not None:
            loss = self._loss(logits, label)
            self._accuracy(logits, label)
            output_dict["loss"] = loss

        return output_dict

    @overrides
    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {
                'accuracy': self._accuracy.get_metric(reset),
                }
