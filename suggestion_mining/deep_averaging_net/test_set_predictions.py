import csv, json
from deep_averaging_net import DeepAveragingNetwork
from sem import SemReader
from sem_predictor import SemPredictor
from pathlib import Path
from tqdm import tqdm
from allennlp.models.archival import Archive, load_archive
from allennlp.predictors.predictor import Predictor


def predict():
    test_file_path = Path('../../Subtask-B/test.jsonl')
    submission_csv_path = Path('../../Subtask-B/submission.csv')

    archive = load_archive('../dan_experiments/model.tar.gz', 0)
    predictor = Predictor.from_archive(archive, 'sem')

    test_json = {
        'sentence_id': 12,
        'sentence': 'i am not asking microsoft to gives permission like android so any app can take my data , but do not keep it restricted like iphone .'
    }

    with test_file_path.open() as test_file, submission_csv_path.open('w') as csvfile:
        lines = test_file.read().splitlines()
        writer = csv.writer(csvfile, quotechar='"', quoting=csv.QUOTE_ALL)
        for line in tqdm(lines):
            json_line = json.loads(line)
            prediction = predictor.predict_json(json_line)
            probs = prediction['class_probabilities']
            predicted_label = probs.index(max(probs))
            writer.writerow([json_line['sentence_id'], json_line['sentence'], predicted_label])

    # prediction = predictor.predict_json(test_json)


if __name__ == '__main__':
    predict()