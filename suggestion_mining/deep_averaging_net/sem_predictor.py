import logging

from typing import Tuple
from overrides import overrides

from allennlp.data import Instance
from allennlp.predictors.predictor import Predictor, JsonDict

logger = logging.getLogger(__name__)


@Predictor.register('sem')
class SemPredictor(Predictor):
    @overrides
    def _json_to_instance(self, json_dict: JsonDict) -> Tuple[Instance, JsonDict]:
        print(json_dict)
        sentence_id = json_dict['sentence_id']
        sentence = json_dict['sentence']
        instance = self._dataset_reader.text_to_instance(sentence=sentence)
        return instance
