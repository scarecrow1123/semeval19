from typing import Dict
import json
import csv
import logging

from overrides import overrides

from allennlp.common.file_utils import cached_path
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.fields import Field, TextField, LabelField, MetadataField
from allennlp.data.instance import Instance
from allennlp.data.token_indexers import SingleIdTokenIndexer, TokenIndexer
from allennlp.data.tokenizers import Tokenizer, WordTokenizer

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


@DatasetReader.register("sem")
class SemReader(DatasetReader):

    def __init__(self,
                 tokenizer: Tokenizer = None,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 lazy: bool = False) -> None:
        super().__init__(lazy)
        self._tokenizer = tokenizer or WordTokenizer()
        self._token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}

    @overrides
    def _read(self, file_path: str):
        # if `file_path` is a URL, redirect to the cache
        file_path = cached_path(file_path)

        with open(file_path, 'r') as sem_file:
            reader = csv.reader(sem_file)
            logger.info("Reading Sem instances from csv dataset at: %s", file_path)
            for record in reader:
                sent_id, sentence, label = record[0], record[1], record[2]

                yield self.text_to_instance(sentence, label)

    @overrides
    def text_to_instance(self,  # type: ignore
                         sentence: str,
                         label: str = None) -> Instance:
        # pylint: disable=arguments-differ
        fields: Dict[str, Field] = {}
        sentence_tokens = self._tokenizer.tokenize(sentence)
        fields['sentence'] = TextField(sentence_tokens, self._token_indexers)
        if label:
            fields['label'] = LabelField(label)

        return Instance(fields)
