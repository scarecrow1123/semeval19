{
  "dataset_reader":{
    "type": "sem",
    "lazy": false,
    "tokenizer": {
      "type": "word",
      "word_splitter": {
        "type": "just_spaces"
      }
    },
    "token_indexers": {
      "tokens": {
        "type": "single_id",
        "lowercase_tokens": true
      },
      //"elmo": {
        //    "type": "elmo_characters"
      //}
    }
  },

  "train_data_path": "train.csv",
  "validation_data_path": "dev.csv",
  
  "model": {
    "type": "dan",
    "text_field_embedder": {
      "tokens": {
        "type": "embedding",
        "pretrained_file": "https://s3-us-west-2.amazonaws.com/allennlp/datasets/glove/glove.840B.300d.txt.gz",
        "embedding_dim": 300,
        "trainable": false,
        "padding_index": 0
      },
      //"elmo":{
        //  "type": "elmo_token_embedder",
          //"options_file": "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
          //"weight_file": "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
          //"do_layer_norm": false,
          //"dropout": 0.0
      //},
    },
    "embedding_dropout": 0.3,
    "feedforward": {
      "input_dim": 300,
      "num_layers": 3,
      "hidden_dims": 300,
      "activations": "relu",
      "dropout": 0.3
    },
    "logits_fn": {
      "input_dim": 300,
      "num_layers": 2,
      "hidden_dims": [128, 2],
      "activations": ["relu", "linear"],
      "dropout": 0.3
    }
  },

  "iterator": {
    "type": "bucket",
    "sorting_keys": [["sentence", "num_tokens"]],
    "batch_size": 200
  },

  "trainer": {
    "optimizer": {
      "type": "adam",
    },
    "num_epochs": 100,
    "patience": 2,
    "cuda_device": 0,
    "validation_metric": "+accuracy"
  }
}