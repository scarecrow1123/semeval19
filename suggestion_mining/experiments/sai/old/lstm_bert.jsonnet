{
  "dataset_reader": {
    "type": "suggestion_mining",
    "tokenizer": {
        "word_splitter": "bert-basic",
        "end_tokens": ["","","","",""],
    },
    "token_indexers": {
      "bert": {
        "type": "bert-pretrained",
        "pretrained_model": "bert-base-uncased",
        "do_lowercase": true
      }
    },
    "max_length": 100
  },
  "train_data_path": "../../data/train.csv",
  "validation_data_path": "../../data/dev.csv",
  "model": {
    "type": "text_classifier",
    "embedding_dropout": 0.5,
    "model_text_field_embedder": {
      "token_embedders": {
        "bert":{
            "type": "bert-pretrained",
            "pretrained_model": "bert-base-uncased",
            "top_layer_only": true
        }
      },
      "embedder_to_indexer_map": {
        "bert": ["bert", "bert-offsets"],   
      },
      "allow_unmatched_keys": true
    },
    "internal_text_encoder": {
      "type": "gru",
      "input_size": 768,
      "hidden_size": 768,
    },
    "classifier_feedforward": {
      "input_dim": 768,
      "num_layers": 3,
      "hidden_dims": [768, 128 , 2],
      "activations": ["relu", "relu", "linear"],
      "dropout":     [0.3, 0.3, 0],
    }
  },
  "iterator": {
    "type": "bucket",
    "sorting_keys": [["text", "num_tokens"]],
    "batch_size": 64,
    "biggest_batch_first": true
  },
  "trainer": {
    "num_epochs": 50,
    "cuda_device": 0,
    "patience": 10,
    "learning_rate_scheduler": {
      "type": "reduce_on_plateau",
      "patience": 3
    },
    "validation_metric": "+accuracy",
    "optimizer": {
      "type": "adam"
    }
  }
}
  