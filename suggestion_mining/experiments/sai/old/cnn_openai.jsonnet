{
  "dataset_reader": {
    "type": "suggestion_mining",
    "tokenizer": {
        "end_tokens": [" "," "," "," "," "],
    },
    "token_indexers": {
      "tokens": {
        "type": "single_id",
        "lowercase_tokens": true
      },
      "openai_transformer": {
        "type": "openai_transformer_byte_pair",
        "model_path": "https://s3-us-west-2.amazonaws.com/allennlp/models/openai-transformer-lm-2018.07.23.tar.gz"
      }
    },
    "max_length": 100
  },
  "train_data_path": "../../data/train.csv",
  "validation_data_path": "../../data/dev.csv",
  "model": {
    "type": "text_classifier",
    "embedding_dropout": 0.5,
    "model_text_field_embedder": {
      "token_embedders": {
        "openai_transformer":{
            "type": "openai_transformer_embedder",
            "transformer": {
                "model_path": "https://s3-us-west-2.amazonaws.com/allennlp/models/openai-transformer-lm-2018.07.23.tar.gz"
            },
         }
      },
      "embedder_to_indexer_map": {
        "openai_transformer": ["openai_transformer", "openai_transformer-offsets"]
      },
      "allow_unmatched_keys": true
    },
    "internal_text_encoder": {
      "type": "cnn",
      "embedding_dim": 768,
      "num_filters": 256,
    },
    "classifier_feedforward": {
      "input_dim": 1024,
      "num_layers": 3,
      "hidden_dims": [1024, 128 , 2],
      "activations": ["relu", "relu", "linear"],
      "dropout":     [0.3, 0.3, 0],
    }
  },
  "iterator": {
    "type": "bucket",
    "sorting_keys": [["text", "num_tokens"]],
    "batch_size": 32,
    "biggest_batch_first": true
  },
  "trainer": {
    "num_epochs": 50,
    "cuda_device": 0,
    "patience": 7,
    "learning_rate_scheduler": {
      "type": "reduce_on_plateau",
      "patience": 3
    },
    "validation_metric": "+accuracy",
    "optimizer": {
      "type": "adam"
    }
  }
}
  