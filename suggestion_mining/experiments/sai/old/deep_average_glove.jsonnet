{
  "dataset_reader": {
    "type": "suggestion_mining"
  },
  "vocabulary": {
    "pretrained_files": {
      "tokens": "https://s3-us-west-2.amazonaws.com/allennlp/datasets/glove/glove.840B.300d.txt.gz"
    },
    "only_include_pretrained_words": true
  },
  "train_data_path": "../../data/train.csv",
  "validation_data_path": "../../data/dev.csv",
  "model": {
    "type": "text_classifier",
    "embedding_dropout": 0.3,
    "model_text_field_embedder": {
      "tokens": {
        "type": "embedding",
        "pretrained_file": "https://s3-us-west-2.amazonaws.com/allennlp/datasets/glove/glove.840B.300d.txt.gz",
        "embedding_dim": 300,
        "trainable": true
      }
    },
    "internal_text_encoder": {
      "type": "boe",
      "embedding_dim": 300,
      "averaged": true
    },
    "classifier_feedforward": {
      "input_dim": 300,
      "num_layers": 3,
      "hidden_dims": [150, 50, 2],
      "activations": ["relu", "relu", "linear"],
      "dropout":     [0.3, 0.3, 0],
    }
  },
  "iterator": {
    "type": "bucket",
    "sorting_keys": [["text", "num_tokens"]],
    "batch_size": 128
  },
  "trainer": {
    "num_epochs": 50,
    "cuda_device": 0,
    "patience": 20,
    "learning_rate_scheduler": {
      "type": "reduce_on_plateau",
      "patience": 3
    },
    "validation_metric": "+accuracy",
    "optimizer": {
      "type": "adam"
    }
  }
}
  