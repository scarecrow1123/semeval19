{
  "dataset_reader": {
    "type": "suggestion_mining",
    "tokenizer": {
        "end_tokens": ["","","","",""],
    },
    "token_indexers": {
      "elmo": {
        "type": "elmo_characters"
      }
    },
    "max_length": 100
  },
  "train_data_path": "../../data/train.csv",
  "validation_data_path": "../../data/dev.csv",
  "model": {
    "type": "text_classifier",
    "embedding_dropout": 0.5,
    "model_text_field_embedder": {
       "elmo":{
        "type": "elmo_token_embedder",
        "options_file": "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
        "weight_file":  "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
        "do_layer_norm": false,
        "dropout": 0.0
      }
    },
    "internal_text_encoder": {
      "type": "cnn",
      "embedding_dim": 1024,
      "num_filters": 256,
    },
    "classifier_feedforward": {
      "input_dim": 1024,
      "num_layers": 3,
      "hidden_dims": [1024, 128 , 2],
      "activations": ["relu", "relu", "linear"],
      "dropout":     [0.3, 0.3, 0],
    }
  },
  "iterator": {
    "type": "bucket",
    "sorting_keys": [["text", "num_tokens"]],
    "batch_size": 64,
    "biggest_batch_first": true
  },
  "trainer": {
    "num_epochs": 50,
    "cuda_device": 0,
    "patience": 7,
    "learning_rate_scheduler": {
      "type": "reduce_on_plateau",
      "patience": 3
    },
    "validation_metric": "+accuracy",
    "optimizer": {
      "type": "adam"
    }
  }
}
  