{
  "dataset_reader": {
    "type": "suggestion_mining",
    "tokenizer": {
        "word_splitter": "bert-basic",
        "end_tokens": ["","","","",""],
    },
    "token_indexers": {
      "bert": {
        "type": "bert-pretrained",
        "pretrained_model": "bert-base-uncased",
        "do_lowercase": true
      }
    },
    "max_length": 100
  },
  "train_data_path": "../../data/train.csv",
  "validation_data_path": "../../data/dev.csv",
  "model": {
    "type": "text_classifier",
    "embedding_dropout": 0.5,
    "model_text_field_embedder": {
      "token_embedders": {
        "bert":{
            "type": "bert-pretrained",
            "pretrained_model": "bert-base-uncased",
            "top_layer_only": true,
            "requires_grad": true
        }
      },
      "embedder_to_indexer_map": {
        "bert": ["bert", "bert-offsets"],   
      },
      "allow_unmatched_keys": true
    },
    "internal_text_encoder": {
      "type": "cnn",
      "embedding_dim": 768,
      "num_filters": 512,
    },
    "classifier_feedforward": {
      "input_dim": 2048,
      "num_layers": 4,
      "hidden_dims": [2048, 1024, 128 , 2],
      "activations": ["relu", "relu", "relu", "linear"],
      "dropout":     [0.1, 0.1, 0.1, 0],
    }
  },
  "iterator": {
    "type": "bucket",
    "sorting_keys": [["text", "num_tokens"]],
    "batch_size": 32,
    "biggest_batch_first": true
  },
  "trainer": {
    "num_epochs": 20,
    "cuda_device": 0,
    "learning_rate_scheduler": {
      "type": "slanted_triangular",
      "num_epochs": 20,
      "num_steps_per_epoch": 200,
      "gradual_unfreezing": true,
      "discriminative_fine_tuning": true,
    },
    "validation_metric": "+accuracy",
    "optimizer": {
      "type": "adam",
      "lr": 0.001,
      "parameter_groups": [
        [["model_text_field_embedder"], {}],
        [["internal_text_encoder"], {}],
        [["classifier_feedforward"], {}]
      ],          
    }
  }
}
  
