{
  "dataset_reader": {
    "type": "suggestion_mining",
    "tokenizer": {
        "word_splitter": "bert-basic",
        "end_tokens": ["","","","",""],
    },
    "token_indexers": {
      "bert": {
        "type": "bert-pretrained",
        "pretrained_model": "bert-base-uncased",
        "do_lowercase": true
      },
      "tokens": {
        "type": "single_id"
      },
    },
    "max_length": 100
  },
  "train_data_path": "../../data/train.csv",
  "validation_data_path": "../../data/dev.csv",
  "model": {
    "type": "text_classifier",
    "embedding_dropout": 0.5,
    "model_text_field_embedder": {
      "token_embedders": {
        "bert":{
            "type": "bert-pretrained",
            "pretrained_model": "bert-base-uncased",
            "top_layer_only": true
        },
        "tokens": {
          "type": "embedding",
          "pretrained_file": "https://s3-us-west-2.amazonaws.com/allennlp/datasets/glove/glove.840B.300d.txt.gz",
          "embedding_dim": 300,
          "trainable": false
        }
      },
      "embedder_to_indexer_map": {
        "bert": ["bert", "bert-offsets"],
        "tokens": ["tokens"],
      },
      "allow_unmatched_keys": true
    },
    "internal_text_encoder": {
      "type": "cnn",
      "embedding_dim": 1068,
      "num_filters": 768,
    },
    "classifier_feedforward": {
      "input_dim": 3072,
      "num_layers": 4,
      "hidden_dims": [3072, 1024, 128 , 2],
      "activations": ["relu", "relu", "relu", "linear"],
      "dropout":     [0.3, 0.3, 0.3, 0],
    }
  },
  "iterator": {
    "type": "bucket",
    "sorting_keys": [["text", "num_tokens"]],
    "batch_size": 64,
    "biggest_batch_first": true
  },
  "trainer": {
    "num_epochs": 50,
    "cuda_device": 0,
    "patience": 10,
    "learning_rate_scheduler": {
      "type": "reduce_on_plateau",
      "patience": 3
    },
    "validation_metric": "+accuracy",
    "optimizer": {
      "type": "adam"
    }
  }
}
  