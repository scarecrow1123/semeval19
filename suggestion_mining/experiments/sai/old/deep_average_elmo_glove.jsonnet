{
  "dataset_reader": {
    "type": "suggestion_mining",
    "token_indexers": {
      "tokens": {
        "type": "single_id",
      },
      "elmo": {
        "type": "elmo_characters",
      },
    },
    "max_length": 100
  },
  "train_data_path": "../../data/train.csv",
  "validation_data_path": "../../data/dev.csv",
  "model": {
    "type": "text_classifier",
    "embedding_dropout": 0.3,
    "model_text_field_embedder": {
       "elmo":{
        "type": "elmo_token_embedder",
        "options_file": "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
        "weight_file":  "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
        "do_layer_norm": false,
        "dropout": 0.0
      },
      "tokens": {
        "type": "embedding",
        "pretrained_file": "https://s3-us-west-2.amazonaws.com/allennlp/datasets/glove/glove.840B.300d.txt.gz",
        "embedding_dim": 300,
        "trainable": false
      }
    },
    "internal_text_encoder": {
      "type": "boe",
      "embedding_dim": 1324,
      "averaged": true
    },
    "classifier_feedforward": {
      "input_dim": 1324,
      "num_layers": 3,
      "hidden_dims": [1324, 128 , 2],
      "activations": ["relu", "relu", "linear"],
      "dropout":     [0.3, 0.3, 0],
    }
  },
  "iterator": {
    "type": "bucket",
    "sorting_keys": [["text", "num_tokens"]],
    "batch_size": 128,
    "biggest_batch_first": true
  },
  "trainer": {
    "num_epochs": 50,
    "cuda_device": 0,
    "patience": 10,
    "learning_rate_scheduler": {
      "type": "reduce_on_plateau",
      "patience": 2
    },
    "validation_metric": "+accuracy",
    "optimizer": {
      "type": "adam"
    }
  }
}
  