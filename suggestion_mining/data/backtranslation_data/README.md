Marian MT models (en to german and german to en) are used to do backtranslation of the training data. 

`input.en` has lines in the same order as that of train.csv and `en.output` has the backtranslated text. Converting to and from original CSV files can be done with the `marian.py` in scripts folder.
