import csv
import argparse
import os
import random
from typing import List, Tuple, T
import textacy
from bs4 import BeautifulSoup
import re

random.seed(1337)

def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-file', type=str, default='original/Training_Full_V1.3.csv')
    parser.add_argument('--output-dir', type=str, default='.')
    parser.add_argument('--dev-file', type=str, default='dev.csv')
    parser.add_argument('--train-file', type=str, default='train.csv')
    parser.add_argument('--unicodize', type=list, default=['original/TrialData_SubtaskA_Test.csv',  'original/TrialData_SubtaskB_Test.csv'])
    parser.add_argument('--dev-ratio', type=float, default=0.1)
    parser.add_argument('--no-shuffle', action='store_true') # Only when the flag is given shuffle will be off
    return parser

def write_data(rows: List[Tuple[str, str, str]], path: str):
    with open(path, 'w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, quoting=csv. QUOTE_ALL)
        for row in rows:
            writer.writerow(row)

def read_and_preprocess_data(path: str) -> List[Tuple[str, str, str]]:
    with open(path, encoding='latin-1') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        rows = []
        for row in csv_reader:
            sent_id, sentence, label = row
            sentence = _normalize_text(sentence)
            rows.append((str(sent_id), str(sentence), str(label)))
    return rows

def remove_duplicates(rows: List[Tuple[str, str, str]]) -> List[Tuple[str, str, str]]:
    prev_sentences = set()
    data = []
    for sent_id, sentence, label in rows:
        if sentence not in prev_sentences:
            prev_sentences |= set([sentence])
            data.append((sent_id, sentence, label))
    print(f'{len(rows) - len(data)} Duplicates removed')
    return data

def split_data(data: List[T], ratio: float, shuffle: bool) -> Tuple[List[T], List[T]]:
    if shuffle:
        random.shuffle(data)
    split_idx = int(len(data) * ratio)
    train_data = data[:-split_idx]
    dev_data = data[-split_idx:]
    return train_data, dev_data

def unicodize(paths: List[str], output_dir: str) -> None:
    for source_path in paths:
        target_path = os.path.join(output_dir, os.path.basename(source_path))
        data = read_and_preprocess_data(source_path)
        write_data(data, target_path)

NUMBER_TOKEN = "@@NUMBER@@"
EMAIL_TOKEN = "@@EMAIL@@"
URL_TOKEN = "@@URL@@"

def _normalize_text(text: str):
    soup = BeautifulSoup(text, features="html5lib")
    text = soup.get_text()
    text = text.replace('\n', ' ').strip('\'').strip('"')
    text = text.replace('___', ' ')
    text = text.replace('__', ' ')
    text = text.replace('""""""', '"')
    text = text.replace('"""""', '"')
    text = text.replace('""""', '"')
    text = text.replace('"""', '"')
    # text = textacy.preprocess.unpack_contractions(text)
    # text = textacy.preprocess.replace_emails(text, replace_with=EMAIL_TOKEN)
    # text = textacy.preprocess.replace_numbers(text, replace_with=NUMBER_TOKEN)
    # text = textacy.preprocess.replace_urls(text, replace_with=URL_TOKEN)
    # text = textacy.preprocess.remove_punct(text, marks=r'`~!#$%^&*()-_=+[]{}\|;:<>/\"')
    # #Not replacing '@' as we use it for delimiter in URL, Number, email tokens
    text = textacy.preprocess.remove_accents(text)
    text = textacy.preprocess.transliterate_unicode(text)
    text = textacy.preprocess.normalize_whitespace(text)
    return text

if __name__ == '__main__':
    args = create_parser().parse_args()
    data = read_and_preprocess_data(args.input_file)
    data = remove_duplicates(data)
    train_data, dev_data = split_data(data, args.dev_ratio, not args.no_shuffle)
    train_path = os.path.join(args.output_dir, args.train_file)
    dev_path = os.path.join(args.output_dir, args.dev_file)
    write_data(train_data, train_path)
    write_data(dev_data, dev_path)
    unicodize(args.unicodize, args.output_dir)
