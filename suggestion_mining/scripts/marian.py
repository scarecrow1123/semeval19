import csv
import logging
import argparse
from tqdm import tqdm
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("op", help="to_marian | to_csv")
parser.add_argument("--csv")
parser.add_argument("--output")
parser.add_argument("--translated-file")

logger = logging.getLogger()


def convert_csv_to_marian(csv_path, output_path):
    with Path(csv_path).open() as csvfile, \
            Path(output_path).open('w') as outputfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')

        logger.info('Writing to %s'.format(output_path))
        for record in tqdm(reader):
            id, text, label = record
            outputfile.write(text + '\n')
        logger.info('Done')

def convert_marian_to_csv(original_csv_path, translated_file_path, output_csv_path):
    with Path(original_csv_path).open() as input_csvfile, \
            Path(translated_file_path).open() as translated_file, \
            Path(output_csv_path).open('w') as output_csvfile:
        reader = csv.reader(input_csvfile, delimiter=',', quotechar='"')
        writer = csv.writer(output_csvfile, quotechar='"', quoting=csv.QUOTE_ALL)
        translated_lines = translated_file.read().splitlines()

        for idx, record in enumerate(reader):
            id, text, label = record
            translated_text = translated_lines[idx]
            translated_id = id + '_translated'
            writer.writerow([translated_id, translated_text, label])


if __name__ == '__main__':
    args = parser.parse_args()
    csv_path = args.csv
    output_path = args.output
    translated_file_path = args.translated_file
    op = args.op

    if op == 'to_marian':
        convert_csv_to_marian(csv_path, output_path)
    elif op == 'to_csv':
        convert_marian_to_csv(csv_path, translated_file_path, output_path)
