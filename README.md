# SemEval19

Code and data for SemEval 2019 submission from ZLabs NLP

Install the dependencies (use conda+pytorch and remaining (allennlp etc) via pip)

## How to train?
Inside the experiments in appropriate directory create config jsonnet and run it like this

PYTHONPATH=../../ allennlp train --include-package=hinton config.jsonnet -s experiment1


## How to predict on test set?
PYTHONPATH=../../ allennlp train --include-package=hinton config.jsonnet -s experiment1

## Experiment organization

Put configuration jsonnets inside an appropriate folder (maybe under your name inside experiments directory).
Write a README.md with description of experiment, what are your expectations before running it, and results.